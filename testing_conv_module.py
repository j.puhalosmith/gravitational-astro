import numpy as np
import conv

fig,ax0 = plt.subplots(2)
ax0[0].plot(time,eleTethysPur[:,0],'r',label="Purterbed Semi Major Axis")
ax0[1].plot(time,eleTethysPur[:,1],'r',label="Purterbed Mean Longitude")
ax0[0].plot(time,eleTethys2body[:,0],'b',label="2 Body Semi Major Axis")
ax0[1].plot(time,eleTethys2body[:,1],'b',label="2 Body Mean Longitude")
fig.legend()

fig,ax1 = plt.subplots(2)
ax1[0].plot(time,eleTethysPur[:,2],'r',label="Purterbed e*sin(varpi)")
ax1[1].plot(time,eleTethysPur[:,3],'r',label="Purterbed e*cos(varpi)")
ax1[0].plot(time,eleTethys2body[:,2],'b',label="2 Body e*sin(varpi)")
ax1[1].plot(time,eleTethys2body[:,3],'b',label="2 Body e*cos(varpi)")
fig.legend()

fig,ax2 = plt.subplots(2)
ax2[0].plot(time,eleTethysPur[:,4],'r',label="Purterbed sin(I/2)*cos(Omega))")
ax2[1].plot(time,eleTethysPur[:,5],'r',label="Purterbed Mean sin(I/2)*sin(Omega) ")
ax2[0].plot(time,eleTethys2body[:,4],'b',label="2 Body sin(I/2)*cos(Omega)")
ax2[1].plot(time,eleTethys2body[:,5],'b',label="2 Body  Mean sin(I/2)*sin(Omega) ")
fig.legend()


print(conv.eletocar.__doc__)
print()
print(conv.cartoele.__doc__)


# Earth 
Rearth = np.array([1,0,0])
Vearth = np.array([0,2*3.14,0])
# Mars
Rmars = np.array([0,3,0])
Vmars = np.array([-3.63,0,0])

# ele = np.array([1,2,3,4,5,6])

EarthGMm = 1.3274625858396057e+20 # 
elepitical = conv.cartoele(Rearth,Vearth,EarthGMm)

print(elepitical)

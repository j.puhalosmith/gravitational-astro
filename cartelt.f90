SUBROUTINE ELETOCAR(ELEM,XYZ,VXYZ,RMU)
      IMPLICIT NONE

! ****************************************************************
! *                                                              *
! * This routine converts the elliptical (keplerian) elements    *
! * into the cartesian ones.                                     *
! * ELEM(1): semimajor axis                                      *
! * ELEM(2): lambda (mean longitude)                             *
! * ELEM(3): e*cos(varpi)                                        *
! * ELEM(4): e*sin(varpi)                                        *
! * ELEM(5): sin(I/2)*cos(Omega)                                 *
! * ELEM(6): sin(I/2)*sin(Omega)                                 *
! *                                                              *
! * XYZ(1): x                                                    *
! * XYZ(2): y                                                    *
! * XYZ(3): z                                                    *
! * VXYZ(1): vx                                                  *
! * VXYZ(2): vy                                                  *
! * VXYZ(3): vz                                                  *
! *                                                              *
! * RMU: G*(m+M)                                                 *
! ****************************************************************      

!f2py      real(KIND=8),DIMENSION(6),intent(in) :: ELEM
real(KIND=8),DIMENSION(6),intent(in) :: ELEM
      real(KIND=8),DIMENSION(6) :: EELEM
!f2py      real(KIND=8),intent(in) :: RMU
      real(KIND=8),intent(in) :: RMU

!f2py      real(KIND=8),DIMENSION(3),intent(out) :: XYZ,VXYZ
      real(KIND=8),DIMENSION(3) :: XYZ,VXYZ

      real(KIND=8) :: DGA,RL,RK,RH,QX,PX,X1,X2,VX1,VY1,AMO,FLE, Y1
      real(KIND=8) :: CF,SF,DLF,RSAM1,ASR,PHI,PSI,RTP,RTQ,RDG,CORF,DCIS2
      integer :: I
      EQUIVALENCE (EELEM(1),DGA),(EELEM(2),RL),(EELEM(3),RK)
      EQUIVALENCE (EELEM(4),RH) ,(EELEM(5),QX),(EELEM(6),PX)
      DO I=1,6
        EELEM(I)=ELEM(I)
      END DO
      AMO=DSQRT(RMU/(DGA*DGA*DGA))
      FLE=RL-RK*DSIN(RL)+RH*DCOS(RL)
20    CONTINUE
        CF=DCOS(FLE)
        SF=DSIN(FLE)
        CORF=(RL-FLE+RK*SF-RH*CF)/(1-RK*CF-RH*SF)
        FLE=FLE+CORF
      IF (DABS(CORF).GE.1.D-14) GOTO 20
      CF=DCOS(FLE)
      SF=DSIN(FLE)
      DLF=-RK*SF+RH*CF
      RSAM1=-RK*CF-RH*SF
      ASR=1.D0/(1.D0+RSAM1)
      PHI=DSQRT(1.D0-RK*RK-RH*RH)
      PSI=1.D0/(1.D0+PHI)
      X1=DGA*(CF-RK-PSI*RH*DLF)
      Y1=DGA*(SF-RH+PSI*RK*DLF)
      VX1=AMO*ASR*DGA*(-SF-PSI*RH*RSAM1)
      VY1=AMO*ASR*DGA*( CF+PSI*RK*RSAM1)
      DCIS2=2.D0*DSQRT(1.D0-PX*PX-QX*QX)
      RTP=1.D0-2.D0*PX*PX
      RTQ=1.D0-2.D0*QX*QX
      RDG=2.D0*PX*QX
      XYZ(1) =      X1 *   RTP   +  Y1 *   RDG
      XYZ(2) =      X1 *   RDG   +  Y1 *   RTQ
      XYZ(3) =  ( - X1 *   PX    +  Y1 *   QX  )  *  DCIS2
      VXYZ(1)=     VX1 *   RTP   + VY1 *   RDG
      VXYZ(2)=     VX1 *   RDG   + VY1 *   RTQ
      VXYZ(3)=  ( -VX1 *   PX    + VY1 *   QX  )  *  DCIS2
      RETURN
      END

! END SUBROUTINE
! ************************************************************************
SUBROUTINE CARTOELE(ELEM,XYZ,VXYZ,RMU)
      
! ****************************************************************
! *                                                              *
! * This routine converts the cartesian  elements                *
! * into the elliptical (keplerian) ones.                        *
! * ELEM(1): semimajor axis                                      *
! * ELEM(2): lambda (mean longitude)                             *
! * ELEM(3): e*cos(varpi)                                        *
! * ELEM(4): e*sin(varpi)                                        *
! * ELEM(5): sin(I/2)*cos(Omega)                                 *
! * ELEM(6): sin(I/2)*sin(Omega)                                 *
! *                                                              *
! * XYZ(1): x                                                    *
! * XYZ(2): y                                                    *
! * XYZ(3): z                                                    *
! * VXYZ(1): vx                                                  *
! * VXYZ(2): vy                                                  *
! * VXYZ(3): vz                                                  *
! *                                                              *
! * RMU: G*(m+M)                                                 *
! ****************************************************************       
      
      
      
      IMPLICIT NONE

!f2py      real(KIND=8),DIMENSION(6),intent(out) :: ELEM
real(KIND=8),DIMENSION(6),intent(out) :: ELEM
      real(KIND=8),DIMENSION(6) :: EELEM
!f2py      real(KIND=8),intent(in) :: RMU
real(KIND=8),intent(in) :: RMU
!f2py      real(KIND=8),DIMENSION(3),intent(in) :: XYZ,VXYZ
real(KIND=8),DIMENSION(3),intent(in) :: XYZ,VXYZ
      real(KIND=8) :: DGA,RL,RK,RH,QX,PX,X1,X2,VX1,VY1,AMO,FLE,Y1,RAY
      real(KIND=8) :: CF,SF,DLF,RSAM1,ASR,PHI,PSI,RTP,RTQ,RDG,CORF,DCIS2
      real(KIND=8) :: GX,GY,GZ,GG,CIS2,ACH,ADG,ACK,SM1,SM2,DET,V2
      integer :: I
      EQUIVALENCE (EELEM(1),DGA),(EELEM(2),RL),(EELEM(3),RK)
      EQUIVALENCE (EELEM(4),RH) ,(EELEM(5),QX),(EELEM(6),PX)
      RAY=DSQRT(XYZ(1)*XYZ(1)+XYZ(2)*XYZ(2)+XYZ(3)*XYZ(3))
      V2=VXYZ(1)*VXYZ(1)+VXYZ(2)*VXYZ(2)+VXYZ(3)*VXYZ(3)
      DGA = RMU * RAY / ( 2.D0*RMU - RAY*V2 )
      GX = XYZ(2)*VXYZ(3) - XYZ(3)*VXYZ(2)
      GY = XYZ(3)*VXYZ(1) - XYZ(1)*VXYZ(3)
      GZ = XYZ(1)*VXYZ(2) - XYZ(2)*VXYZ(1)
      GG = DSQRT(GX*GX+GY*GY+GZ*GZ)
      CIS2 = DSQRT( ( 1.D0 + GZ/GG ) / 2.D0 )
      QX = - GY / ( 2.D0 * GG * CIS2 )
      PX =   GX / ( 2.D0 * GG * CIS2 )
      RTP=1.D0-2.D0*PX*PX
      RTQ=1.D0-2.D0*QX*QX
      RDG=2.D0*PX*QX
      X1  = RTP *  XYZ(1) + RDG *  XYZ(2) -2.D0*PX*CIS2 *  XYZ(3)
      Y1  = RDG *  XYZ(1) + RTQ *  XYZ(2) +2.D0*QX*CIS2 *  XYZ(3) 
      VX1 = RTP * VXYZ(1) + RDG * VXYZ(2) -2.D0*PX*CIS2 * VXYZ(3)
      VY1 = RDG * VXYZ(1) + RTQ * VXYZ(2) +2.D0*QX*CIS2 * VXYZ(3) 
      RK =  GG * VY1 /RMU  -  X1 / RAY
      RH = -GG * VX1 /RMU  -  Y1 / RAY
      PSI=1.D0/(1.D0+DSQRT(1.D0-RK*RK-RH*RH))
      ACH=1.D0-PSI*RH*RH
      ADG=PSI*RH*RK
      ACK=1.D0-PSI*RK*RK
      DET=ACH*ACK-ADG*ADG
      SM1=X1/DGA + RK
      SM2=Y1/DGA + RH
      CF=(SM1*ACK-SM2*ADG)/DET
      SF=(ACH*SM2-ADG*SM1)/DET
! c             WRITE(*,*) 'controle ',CF*CF+SF*SF-1.D0
      FLE=ATAN2(SF,CF)
      RL = FLE - RK * SF + RH * CF
      ! RL = atan2()
      DO I=1,6
        ELEM(I)=EELEM(I)
      END DO
      RETURN
      END
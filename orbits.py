import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import argrelextrema
import re

def orbits(files = ['dataMimas.dat', 'dataTethys.dat', 'dataTitan.dat', 'dataEnergy.dat'],plot3d = False, trajectory = False,
           velocities = False, period = False, plot_energy = False,
           compare = False, longitude = False,
           compare_files = ['dataMimas.dat', 'dataTethys.dat', 'dataTitan.dat', 'dataEnergy.dat']):
    """
    distances = True, then it plots the distance between satellites
    plot3d = True, then it plots the 3d trajectories
    trajectory = True, then it plots the trajectories for x, y, z
    velocities = True, then it plots the velocities for x, y, z
    period = True, then it gets and compares the period
    plot_energy = True, then it plots the energy evolution
    compare = True, then it compares the real trajectory and velocities with the calculated ones
    longitude = True, then it get the longitude of the pericenter and the ascending node
    compare_files = Set of files whose trajectory error you want to compare with the orginal files
                    So, you should run one potential, rename its files, then run the integrator with
                    another potential and compare them
    """
    
    """
    Getting the data from the files
    """
    data1 = np.loadtxt(files[0])
    data2 = np.loadtxt(files[1])
    data3 = np.loadtxt(files[2])
    data4 = np.loadtxt(files[3])  # for the energies
    au = 149597870700  # astronomical units
    jour = 24*3600  # dias
    
    time = data1[:, 0]/jour
    print(len(time))
    x1 = data1[:, 1]/au
    y1 = data1[:, 2]/au
    z1 = data1[:, 3]/au
    vx1 = data1[:, 1]/au*jour
    vy1 = data1[:, 2]/au*jour
    vz1 = data1[:, 3]/au*jour
    
    x2 = data2[:, 1]/au
    y2 = data2[:, 2]/au
    z2 = data2[:, 3]/au
    vx2 = data2[:, 1]/au*jour
    vy2 = data2[:, 2]/au*jour
    vz2 = data2[:, 3]/au*jour
    
    
    x3 = data3[:, 1]/au
    y3 = data3[:, 2]/au
    z3 = data3[:, 3]/au
    vx3 = data3[:, 1]/au*jour
    vy3 = data3[:, 2]/au*jour
    vz3 = data3[:, 3]/au*jour
    
    energy = data4[:, 1]
    

    if plot3d == True:
        """
        Ploting the 3d trajectory
        """
        ax = plt.figure().add_subplot(projection='3d')
        ax.plot(x1, y1, z1, label='Mimas')
        ax.plot(x2, y2, z2, label='Thetis')
        ax.plot(x3, y3, z3, label='Titan')
        ax.legend()
        ax.set_ylabel('$X$')
        ax.set_ylabel('$Y$')
        ax.set_ylabel('$Z$')
    
    """
    Ploting the temporal evolution of x, y, x
    """
    if trajectory  == True:
        fig, axs = plt.subplots(3, sharex=True, sharey=True)
        fig.suptitle('Trajectories')
        axs[0].plot(time, x1, time, x2, time, x3)
        axs[1].plot(time, y1, time, y2, time, y3)
        axs[2].plot(time, z1, time, z2, time, z3)
        axs[0].set_ylabel('$X$')
        axs[1].set_ylabel('$Y$')
        axs[2].set_ylabel('$Z$')
        fig.supxlabel('time [days]')
        fig.supylabel('distance [au]')
    
    """
    Ploting the temporal evolution of the velocities
    """
    if velocities == True:
        fig1, axs1 = plt.subplots(3)
        fig1.suptitle('Velocities')
        axs1[0].plot(time, vx1, time, vx2, time, vx3)
        axs1[1].plot(time, vy1, time, vy2, time, vy3)
        axs1[2].plot(time, vz1, time, vz2, time, vz3)
        axs1[0].set_ylabel('$X$')
        axs1[1].set_ylabel('$Y$')
        axs1[2].set_ylabel('$Z$')
        fig1.supxlabel('time [days]')
        fig1.supylabel('distance [au]')
        
    """
    Period calculation
    """
    # I compute the distance between maximums and I do the average between them
    # Since we acumulate error with the time, maybe I should take the first maximums and not all
    # Anyway, I think this doesn't affect the result almost nothing...
    
    if period == True:
        def period(time, x1):
            peaks = np.where((x1[1:-1] > x1[0:-2]) * (x1[1:-1] > x1[2:]))[0] + 1
            periodo = time[peaks]
            p = []
            for i in range(len(periodo)-1):
                p.append(periodo[i+1]-periodo[i])
            return p
        
        
        T1 = np.mean(period(time, x1))
        T2 = np.mean(period(time, x2))
        T3 = np.mean(period(time, x3))
        T1err = np.std(period(time, x1))
        T2err = np.std(period(time, x2))
        T3err = np.std(period(time, x3))
        [T1real, T2real, T3real] = [0.9424218, 1.888, 15.945421]
        print('period Mimas : ', T1, '+-', T1err, 'days')
        print('period Thetis : ', T2, '+-', T2err, 'days')
        print('period Titan : ', T3, '+-', T3err, 'days')
        print('Real periods are : ', T1real, T2real, T3real)
    
    """
    Energy
    We expect some small oscillations and the energy tends to decrease.
    However, those oscillations are very small in comapare with the total energy,
    and this decreasing process is due to the time step, because you can check that 
    if you reduce the time step you also reduce this variation in the energy.
    In conclusion: Even if you observe variations, don't worry those are only in 
    the last decimals, good job!
    """
    if plot_energy == True:
        plt.figure()
        plt.plot(time, energy)
        plt.title('Total energy for all bodies')
        plt.xlabel = 'time [days]'
        plt.ylabel = 'Energy [J]'  # Should I use another unit??
        # plt.ylim([np.mean(energy)-np.mean(energy)*0.01,
                 # np.mean(energy)+np.mean(energy)*0.01])
    
    
    """
    Real data from the text files. You need to have mimas.txt, tethis.txt and
    titan.txt in the same repository as the code
    """
    if compare == True:
        
        def reading(file='mimas.txt'):
            file = open(file, 'r')
            lines = file.readlines()
            count = 0
            # Strips the newline character
            x = []
            y = []
            z = []
            vx = []
            vy = []
            vz = []
            for line in lines:
                # print(line)
                count += 1
                if line.startswith(' X'):
                    data = re.findall(r'-?\d+\.{0,1}\d*', line)
                    x.append(float(data[0])*10**float(data[1])*1000/au)
                    y.append(float(data[2])*10**float(data[3])*1000/au)
                    z.append(float(data[4])*10**float(data[5])*1000/au)
                elif line.startswith(' VX'):
                    data1 = re.findall(r'-?\d+\.{0,1}\d*', line)
                    vx.append(float(data1[0])*10**float(data1[1])*1000/au)
                    vy.append(float(data1[2])*10**float(data1[3])*1000/au)
                    vz.append(float(data1[4])*10**float(data1[5])*1000/au)
            return x, y, z, vx, vy, vz
        
        
        """
        Read the real data and plot it
        """
        [x_mimas, y_mimas, z_mimas, vx_mimas,
            vy_mimas, vz_mimas] = reading('mimas.txt')
        [x_tethis, y_tethis, z_tethis, vx_tethis,
            vy_tethis, vz_tethis] = reading('tethis.txt')
        [x_titan, y_titan, z_titan, vx_titan,
            vy_titan, vz_titan] = reading('titan.txt')
        time_real = np.arange(len(x_mimas))  # we have one data per day
        
        # Plot of the real trajectories
        fig_real, axs_real = plt.subplots(3, sharex=True, sharey=True)
        fig_real.suptitle('Real trajectories')
        axs_real[0].plot(time_real, x_mimas, '*', time_real,
                         x_tethis, '+', time_real, x_titan, 'o')
        axs_real[1].plot(time_real, y_mimas, '*', time_real,
                         y_tethis, '+', time_real, y_titan, 'o')
        axs_real[2].plot(time_real, z_mimas, '*', time_real,
                         z_tethis, '+', time_real, z_titan, 'o')
        axs_real[0].set_ylabel('$X$')
        axs_real[1].set_ylabel('$Y$')
        axs_real[2].set_ylabel('$Z$')
        fig_real.supxlabel('time [days]')
        fig_real.supylabel('distance [au]')
        
        # Plot of the real velocities
        fig_vreal, axs_vreal = plt.subplots(3, sharex=True, sharey=True)
        fig_vreal.suptitle('Real velocities')
        axs_vreal[0].plot(time_real, vx_mimas, '*', time_real,
                          vx_tethis, '+', time_real, vx_titan, 'o')
        axs_vreal[1].plot(time_real, vy_mimas, '*', time_real,
                          vy_tethis, '+', time_real, vy_titan, 'o')
        axs_vreal[2].plot(time_real, vz_mimas, '*', time_real,
                          vz_tethis, '+', time_real, vz_titan, 'o')
        axs_vreal[0].set_ylabel('$X$')
        axs_vreal[1].set_ylabel('$Y$')
        axs_vreal[2].set_ylabel('$Z$')
        fig_vreal.supxlabel('time [days]')
        fig_vreal.supylabel('velocity [au/days]')
        """
        Other trajectory to compare
        """
        data1_compare = np.loadtxt(compare_files[0])
        data2_compare = np.loadtxt(compare_files[1])
        data3_compare = np.loadtxt(compare_files[2])
        data4_compare = np.loadtxt(compare_files[3])  # for the energies
        au = 149597870700  # astronomical units
        jour = 24*3600  # days
        
        time_compare = data1_compare[:, 0]/jour
        x1_compare = data1_compare[:, 1]/au
        y1_compare = data1_compare[:, 2]/au
        z1_compare = data1_compare[:, 3]/au
       
        x2_compare = data2_compare[:, 1]/au
        y2_compare = data2_compare[:, 2]/au
        z2_compare = data2_compare[:, 3]/au

        x3_compare = data3_compare[:, 1]/au
        y3_compare = data3_compare[:, 2]/au
        z3_compare = data3_compare[:, 3]/au

        
        """
        Comparision between real and computed trajectory
        """
        
        
        def compara(time_real, x_real, y_real, z_real,
                    time, x, y, z, 
                    time_compare, x_compare, y_compare, z_compare,
                    name='the moon', magnitude='trajectory'):
            
            # plots with the trajectories
            fig_compare, axs_compare = plt.subplots(3, sharex=True, sharey=True)
            fig_compare.suptitle('Real vs computed ' + magnitude + ' of '+name)
            axs_compare[0].plot(time_real, x_real, '*', time, x)
            axs_compare[1].plot(time_real, y_real, '*', time, y)
            axs_compare[2].plot(time_real, z_real, '*', time, z)
            axs_compare[0].set_ylabel('$X$')
            axs_compare[1].set_ylabel('$Y$')
            axs_compare[2].set_ylabel('$Z$')
            fig_compare.supxlabel('time [days]')
            fig_compare.supylabel('distance [au]')
            # plots with the errors
            # I interpolate the position at the exact time of the measurement
            errx = np.abs(x_real - np.interp(time_real, time, x))
            erry = np.abs(y_real - np.interp(time_real, time, y))
            errz = np.abs(z_real - np.interp(time_real, time, z))
            errdistance = (errx**2+erry**2+errz**2)**0.5
            # First element fails, not a big deal, but I deleted it
            fig_err, axs_err = plt.subplots(1, sharex=True, sharey=True)
            fig_err.suptitle('Error in the calculated trajectory of '+name)
            axs_err.bar(time_real[1:], errdistance[1:])
            fig_err.supxlabel('time [days]')
            fig_err.supylabel('error [au]')
            #Add the compared potential
            errx_compare = np.abs(x_real - np.interp(time_real, time_compare, x_compare))
            erry_compare = np.abs(y_real - np.interp(time_real, time_compare, y_compare))
            errz_compare = np.abs(z_real - np.interp(time_real, time_compare, z_compare))
            errdistance_compare = (errx_compare**2+erry_compare**2+errz_compare**2)**0.5
            axs_err.bar(time_real[1:], errdistance_compare[1:], color = 'red')
        
        index = np.argmin(np.abs(np.array(time)-time_real[-1]))
        time_compara = time[0:index+1]
        time_compare = time_compare[0:index+1]
        compara(time_real, x_titan, y_titan, z_titan, time_compara,
                x3[0:index+1], y3[0:index+1], z3[0:index+1],
                time_compare, x3_compare[0:index+1], y3_compare[0:index+1], 
                z3_compare[0:index+1], name='Titan')
        compara(time_real, x_tethis, y_tethis, z_tethis, time_compara,
                x2[0:index+1], y2[0:index+1], z2[0:index+1],
                time_compare, x2_compare[0:index+1], y2_compare[0:index+1], 
                z2_compare[0:index+1], name='Tethis')
        compara(time_real, x_mimas, y_mimas, z_mimas, time_compara,
                x1[0:index+1], y1[0:index+1], z1[0:index+1],
                time_compare, x1_compare[0:index+1], y1_compare[0:index+1], 
                z1_compare[0:index+1], name='Mimas')

        
    """
    %matplotlib
    """
    if longitude == True:

        n1 = 2*np.pi/T1real
        n2 = 2*np.pi/T2real
        n3 = 2*np.pi/T3real
        J2 = 1.629*10**(-2)
        
        R = 60268  # Saturn’s equatorial radius km
        # a = semimajor axis
        a1 = 185.54*10**3
        a2 = 294.670*10**3
        a3 = 1221.87*10**3
        
        omega_bar = [1.5*J2*(R/a1)**2*n1, 1.5*J2*(R/a2)**2*n2, 1.5*J2*(R/a3)**2*n3]
        rho = [-1.5*J2*(R/a1)**2*n1, -1.5*J2*(R/a2)**2*n2, -1.5*J2*(R/a3)**2*n3]
        print('Theorical (RHS of equation in practical) longitude of the pericenter (omega_bars) are : ')
        print(np.round(omega_bar, 5))
        print('Theorical longitude of the ascending node (rho) are : ')
        print(np.round(rho, 5))

"""
orbits(distances = True, plot3d = True, trajectory = True,
           velocities = True, period = True, plot_energy = True,
           compare = True, longitude = True)
"""
#For the testing parameters:
# call th_body(h=1.0_dp, steps=6000000, pert=5)
#I used these parameters on Fortran, I should modify it to put it on Jupiter
# orbits(distances=True,trajectory = True)

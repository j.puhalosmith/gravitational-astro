! This code integrates the orbits of the the Saturn system with three of its satellites, Mimas, Tethys, and Titan.
! Saturn is supposed to be at the center.
! M1: Mimas
! M2: Tethys
! M3: Titan

! program magnitudes
!     implicit none
!     integer, parameter :: dp = SELECTED_REAL_KIND(P=8)
    
!     call th_body(h=1.e-1_dp, steps=10000000, pert=0)

! contains

subroutine th_body(h, steps, pert)
    implicit none
    integer, parameter :: dp = SELECTED_REAL_KIND(P=8)

    ! Math constants
    real (KIND=dp), parameter :: pi = 3.1415926535897932384_dp
    real (KIND=dp), parameter :: G  = 6.674e-11_dp  
    real (KIND=dp)            :: M  = 568.32e24_dp              ! Saturn's mass [kg]
    real (KIND=dp)            :: GM                             ! Saturn's gravitational constant [m^3/kg/s^2]
    
    ! RK4 constants and variables
    real (KIND=dp), dimension(3,6) :: k0, k1, k2, k3, k4
    real (KIND=dp)                 :: h               !RK4 time-step size [s]
    !f2py intent(in)               :: h
    integer                        :: steps               !Number of steps
    !f2py intent(in)               :: steps
    integer                        :: pert                !Perturbation flag
    !f2py intent(in)               :: pert
    ! Declaration of sattelites' variables
    real (KIND=dp), allocatable :: R(:,:), V(:,:)               !1st dimension: 3 bodies, 2nd dimension: 3 coordinates
    real (kind=dp), allocatable :: R_norm(:), V_norm(:)         !Norm of R and V for each body
    real (KIND=dp)              :: GM1, GM2, GM3, M1, M2, M3
    real (KIND=dp)              :: t, e_tot, e_kin, e_pot, e_per

    ! Other variables
    integer :: i, j, k!, body_num!, codeError, io
    character (len=20) :: out_1, out_2, out_3, out_4


    ! ==================== Initial Condition =====================================
    t = 0.e0_dp

    allocate(R(3,3), V(3,3), R_norm(3) , V_norm(3))

    ! Initial positions of Mimas, Tethys, and Titan [m]
    R(1,1) = -1.155743126780157e8_dp
    R(1,2) =  1.325405469178612e8_dp
    R(1,3) = -6.405744636561111e7_dp

    R(2,1) =  2.736338967931043e7_dp
    R(2,2) = -2.583656101498802e8_dp
    R(2,3) =  1.389825768439401e8_dp

    R(3,1) =  1.022946170421365e9_dp
    R(3,2) =  5.037897354451453e8_dp 
    R(3,3) = -3.617275927456587e8_dp

    ! Initial velocities of Mimas, Tethys, and Titan [m/s]
    V(1,1) = -1.127495088210949e4_dp
    V(1,2) = -7.155743126780157e3_dp
    V(1,3) =  4.806656698122779e3_dp

    V(2,1) =  1.126007562976311e4_dp 
    V(2,2) =  4.553114846840171e2_dp 
    V(2,3) = -1.372033455417533e3_dp
 
    V(3,1) = -2.810756941076592e3_dp 
    V(3,2) =  4.506080188745751e3_dp 
    V(3,3) = -2.043514745714397e3_dp

    M1 = 3.75e19_dp   ! Mimas' mass [kg]
    M2 = 61.76e19_dp  ! Tethys' mass [kg]
    M3 = 1.3455e23_dp ! Titan's mass [kg]

if (pert==5) then
        ! Initial positions of Mimas, Tethys, and Titan [m]
        ! modified for a test removing Saturn
        R(1,1) = 1e8_dp
        R(1,2) = 1e8_dp
        R(1,3) = 1e8_dp

        R(2,1) = 2e8_dp
        R(2,2) = 2e8_dp
        R(2,3) = 2e8_dp

        R(3,1) = 3e8_dp
        R(3,2) = 3e8_dp
        R(3,3) = 3e8_dp

        ! Initial velocities of Mimas, Tethys, and Titan [m/s]
        V(1,1) = 0.0_dp
        V(1,2) = 0.0_dp
        V(1,3) = 0.0_dp

        V(2,1) = 0.0_dp 
        V(2,2) = 0.0_dp 
        V(2,3) = 0.0_dp
    
        V(3,1) = 0.0_dp 
        V(3,2) = 0.0_dp
        V(3,3) = 0.0_dp

        M1 = 1.0e21_dp   ! test' mass [kg]
        M2 = 1.0e21_dp  ! test' mass [kg]
        M3 = 1.0e21_dp ! test's mass [kg]
        M = 0.0_dp
    end if
 
    R_norm(:) = norm2(R(:,:), dim=2)
    V_norm(:) = norm2(V(:,:), dim=2)    
    GM  = G*M
    GM1 = G*M1    
    GM2 = G*M2    
    GM3 = G*M3    
    
    ! ==================== RK4 implementation =====================================

    ! Open output files
    out_1 = 'dataMimas.dat'
    out_2 = 'dataTethys.dat'
    out_3 = 'dataTitan.dat'
    out_4 = 'dataEnergy.dat'
    open(unit=100, file=out_1)
    open(unit=200, file=out_2)
    open(unit=300, file=out_3)
    open(unit=400, file=out_4)

    write(100,*) "#  0.Time  1.x1  2.y1  3.z1  4.Vx1  5.Vy1  6.Vz1  7.R1  8.V1"
    write(200,*) "#  0.Time  1.x2  2.y2  3.z2  4.Vx2  5.Vy2  6.Vz2  7.R2  8.V2"
    write(300,*) "#  0.Time  1.x3  2.y3  3.z3  4.Vx3  5.Vy3  6.Vz3  7.R3  8.V3"
    write(400,*) "#  0.Time  1.e_tot  2.e_kin  3.e_pot  4.e_per"

    ! Initialization of k-values
    k0(:,:) = 0.e0_dp    ! This table of zeros will be used only to make k1.

    ! main loop
    do i=1, steps-1
        call k_sub(GM, GM1, GM2, GM3, h, V(:,:), R(:,:),         k0(:,:), k1(:,:), pert)    ! k1 for all sattelites
        call k_sub(GM, GM1, GM2, GM3, h, V(:,:), R(:,:),         k1(:,:), k2(:,:), pert)    ! k2 for all sattelites
        call k_sub(GM, GM1, GM2, GM3, h, V(:,:), R(:,:),         k2(:,:), k3(:,:), pert)    ! k3 for all sattelites
        call k_sub(GM, GM1, GM2, GM3, h, V(:,:), R(:,:), 2.e0_dp*k3(:,:), k4(:,:), pert)    ! k4 for all sattelites

        ! update of position and velocity
            do k=1, 3
        do j=1, 3
                R(j,k) = R(j,k) + ( h * (k1(j,k)   + 2.e0_dp*k2(j,k)   + 2.e0_dp*k3(j,k)   + k4(j,k)   ) )/6.e0_dp
                V(j,k) = V(j,k) + ( h * (k1(j,k+3) + 2.e0_dp*k2(j,k+3) + 2.e0_dp*k3(j,k+3) + k4(j,k+3) ) )/6.e0_dp
            end do
        end do

        ! Position and velocity magnitudes for the 3 sattelites
        R_norm(:) = norm2(R(:,:), dim=2)
        V_norm(:) = norm2(V(:,:), dim=2)

        ! Update of time
        t = t+h
        
        ! Energy of the system
            e_kin = 0.5 * M1 * V_norm(1)**2 &
                  + 0.5 * M2 * V_norm(2)**2 &
                  + 0.5 * M3 * V_norm(3)**2
            
            e_pot = -GM * (M1 / R_norm(1) + &
                           M2 / R_norm(2) + &
                           M3 / R_norm(3))
            
            if (pert==0) then
                e_per = 0.e0_dp
            else if (pert==1 .or. pert==2) then
                e_per = -GM1 * M2 / sqrt( (R(1,1)-R(2,1))**2 + (R(1,2)-R(2,2))**2 + (R(1,3)-R(2,3))**2 ) &
                        -GM1 * M3 / sqrt( (R(1,1)-R(3,1))**2 + (R(1,2)-R(3,2))**2 + (R(1,3)-R(3,3))**2 ) &
                        -GM2 * M3 / sqrt( (R(2,1)-R(3,1))**2 + (R(2,2)-R(3,2))**2 + (R(2,3)-R(3,3))**2 )
            end if

            e_tot = e_kin + e_pot + e_per

        ! Print data to files
        if (MOD(i,1000)==0) then      ! Print data every 100000 steps
            write(100,*) t, R(1,1), R(1,2), R(1,3), V(1,1), V(1,2), V(1,3), R_norm(1), V_norm(1)
            write(200,*) t, R(2,1), R(2,2), R(2,3), V(2,1), V(2,2), V(2,3), R_norm(2), V_norm(2)
            write(300,*) t, R(3,1), R(3,2), R(3,3), V(3,1), V(3,2), V(3,3), R_norm(3), V_norm(3)
            write(400,*) t, e_tot, e_kin, e_pot, e_per
        end if

    end do

    close(100)
    close(200)
    close(300)
    close(400)
    deallocate(R, V, R_norm, V_norm)
    print*, "--------------------------------------------------------------------"
    print*, "Done! Outputs are saved in files: "
    print*, out_1, out_2, out_3, out_4
    print*, "--------------------------------------------------------------------"
end subroutine th_body


subroutine k_sub(GM, GM1, GM2, GM3, h, velocity, position, k_in, k_out, pert)
    ! This subroutine produces 6 "k"s for each body
    implicit none
    integer, parameter                 :: dp = SELECTED_REAL_KIND(P=8)
    integer, parameter                 :: nbody = 3         !number of bodies
    real (KIND=dp), dimension(nbody,3) :: position, velocity
    !f2py intent(in)                   :: position, velocity
    real (KIND=dp), dimension(nbody,6) :: k_in
    !f2py intent(in)                   :: k_in
    real (KIND=dp), dimension(nbody,6) :: k_out             !vector k with 6 components
    !f2py intent(out)                  :: k_out
    real (KIND=dp)                     :: GM, GM1, GM2, GM3
    !f2py intent(in)                   :: GM, GM1, GM2, GM3
    real (KIND=dp)                     :: h                 !RK4 time-step size [s]
    !f2py intent(in)                   :: h
    integer                            :: pert              !perturbation flag
    !f2py intent(in)                   :: pert
    real (KIND=dp), dimension(nbody,3) :: R_vec2
    real (kind=dp), dimension(nbody)   :: position_norm, flatX, flatY, flatZ
    real (kind=dp), dimension(nbody)   :: d_12, d_13, d_23
    real (KIND=dp)                     :: d_12_norm, d_13_norm, d_23_norm, GMM
    real (KIND=dp), parameter          :: j2 = 1.629e-2_dp
    real (KIND=dp), parameter          :: R_saturn = 60268e3_dp ! Saturn radius [m]
    INTEGER                            :: i, j

    ! Making k(i,j) for each body. For each body we need to compute 6 k values (j=1:6)
    ! i: the body number (1 for Mimas, 2 for Tethys and 3 for Titan), j=1:3 the , j=4:6 is the velocity vector.

    ! k(:,1:3)
    ! For k1(1:3), by putting a zero array instead of k_in the 2nd term on the rhs vanishes.
    do j=1, 3
        k_out(1,j) = velocity(1,j) + h * k_in(1,j+3) / 2.e0_dp
        k_out(2,j) = velocity(2,j) + h * k_in(2,j+3) / 2.e0_dp
        k_out(3,j) = velocity(3,j) + h * k_in(3,j+3) / 2.e0_dp
    end do

    ! k(:,4:6)

    ! This loop is funny! Only makes the position vectors to be used for making k2(1:3), k3(1:2), k4(1:2).
    ! So, not a big deal although it might seem crazy why I did this way :D
    do i=1, 3
        R_vec2(1,i) = position(1,i) + h * k_in(1,i) / 2.e0_dp
        R_vec2(2,i) = position(2,i) + h * k_in(2,i) / 2.e0_dp
        R_vec2(3,i) = position(3,i) + h * k_in(3,i) / 2.e0_dp
        
        d_12(i) = R_vec2(2,i) - R_vec2(1,i)
        d_13(i) = R_vec2(3,i) - R_vec2(1,i)
        d_23(i) = R_vec2(3,i) - R_vec2(2,i)
    end do

    ! The norm of the 3 position vectors to be used in the denominators of k
    position_norm(:) = norm2(R_vec2, dim=2)
    d_12_norm = norm2(d_12)
    d_13_norm = norm2(d_13)
    d_23_norm = norm2(d_23)

    do i=1, 3
        if (i==1) then
            GMM = GM1
        else if (i==2) then
            GMM = GM2
        else if (i==3) then
            GMM = GM3
        end if
        flatX(i) = - 3./2.* (GM+GMM) * J2 * R_saturn**2 * R_vec2(i,1) * (R_vec2(i,1)**2&
                   + R_vec2(i,2)**2 - 4 * R_vec2(i,3)**2) / position_norm(i)**7
        flatY(i) = - 3./2.* (GM+GMM) * J2 * R_saturn**2 * R_vec2(i,2) * (R_vec2(i,1)**2&
                   + R_vec2(i,2)**2 - 4 * R_vec2(i,3)**2) / position_norm(i)**7
        flatZ(i) = - 3./2.* (GM+GMM) * J2 * R_saturn**2 * R_vec2(i,3) * (3* R_vec2(i,1)**2&
                   + 3*R_vec2(i,2)**2 - 2 * R_vec2(i,3)**2) / position_norm(i)**7
    end do

    do j=4, 6
        if (pert==0) then
            k_out(1,j) = - GM  * R_vec2(1,j-3) / position_norm(1) **3
            k_out(2,j) = - GM  * R_vec2(2,j-3) / position_norm(2) **3
            k_out(3,j) = - GM  * R_vec2(3,j-3) / position_norm(3) **3
        else if (pert==1 .or. pert==5) then
            k_out(1,j) = - GM  * R_vec2(1,j-3) / position_norm(1) **3 &
                         + GM2 * d_12(j-3)     / d_12_norm        **3 &
                         + GM3 * d_13(j-3)     / d_13_norm        **3
                        
            k_out(2,j) = - GM  * R_vec2(2,j-3) / position_norm(2) **3 &
                         - GM1 * d_12(j-3)     / d_12_norm        **3 &
                         + GM3 * d_23(j-3)     / d_23_norm        **3
            
            k_out(3,j) = - GM  * R_vec2(3,j-3) / position_norm(3) **3 &
                         - GM1 * d_13(j-3)     / d_13_norm        **3 &
                         - GM2 * d_23(j-3)     / d_23_norm        **3
        else if (pert==2) then
            k_out(1,j) = - GM  * R_vec2(1,j-3) / position_norm(1) **3 &
                         + GM2 * d_12(j-3)     / d_12_norm        **3 &
                         + GM3 * d_13(j-3)     / d_13_norm        **3 &
                         + flatX(j-3)
            k_out(2,j) = - GM  * R_vec2(2,j-3) / position_norm(2) **3 &
                         - GM1 * d_12(j-3)     / d_12_norm        **3 &
                         + GM3 * d_23(j-3)     / d_23_norm        **3 &
                         + flatY(j-3)
            k_out(3,j) = - GM  * R_vec2(3,j-3) / position_norm(3) **3 &
                         - GM1 * d_13(j-3)     / d_13_norm        **3 &
                         - GM2 * d_23(j-3)     / d_23_norm        **3 &
                         + flatZ(j-3)
        end if
    end do

end subroutine k_sub

! end program magnitudes